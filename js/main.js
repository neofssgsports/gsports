$(document).ready(function(){
	$('.show-mob-men').on('click',function(e){
		e.stopPropagation();
  		e.preventDefault();
  		var menuTarget = $(this).data('target');
  		checkMobileMenu(menuTarget);
	});
	$('.modal-controller').on('click',function(e){
		e.stopPropagation();
  		e.preventDefault();
  		var modalTarget = $(this).data('target');
  		checkModalStatus(modalTarget);
	});
	$('.closeMe').on('click',function(e){
		e.stopPropagation();
  		e.preventDefault();
  		closeMobileMenu(this);
	});
	$('.close-modal').on('click',function(e){
		e.stopPropagation();
  		e.preventDefault();
  		closeModal(this);
	});
	if($(document).width() <= 991){
		$(document).mouseup(function (e)
		{
		    var container = $(".mobile_hidden_links");

		    if (!container.is(e.target) // if the target of the click isn't the container...
		        && container.has(e.target).length === 0) // ... nor a descendant of the container
		    {
		        container.removeClass('active');
		        $('body').removeClass('activeMobileMenu');
		    }
		});
	}
	$('.numbersOnly').on('keydown',function(e){
		// Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
	});
	
});

$(window).load(function(){
	if($('.sticky-sidebar').length){
		calculateStickyPos();
	}
}).scroll(function(){
	if($('.sticky-sidebar').length){
		calculateStickyPos();
	}
});
if($('.sticky-sidebar').length){
	var stickyOrigPos = $('.sticky-sidebar').offset().top;
}
function calculateStickyPos(){
	var documentTop = $(window).scrollTop();
	var stickyStarter = $('.sticky-sidebar-guide').offset().top;
	var startStick = stickyStarter - documentTop;
	var stickAmout = documentTop + 100;
	var stickAmoutTotal = (documentTop - stickyOrigPos) + 60;
	if(startStick < 100 && stickAmoutTotal > 0){
		$('.sticky-sidebar').css({transform:'translate(0,'+stickAmoutTotal+'px)'});
	}else{
		$('.sticky-sidebar').css({transform:'translate(0,0)'});
	}
}

function checkModalStatus(target){
	$('body').addClass('activeMobileMenu');
	$('.quick-access-modal[data-identity='+target+']').addClass('active');
}
function checkMobileMenu(target){
	$('body').addClass('activeMobileMenu');
	$('.mobile_hidden_links[data-identity='+target+']').addClass('active');
}
function closeModal(selector){
	$('body').removeClass('activeMobileMenu');
	$(selector).closest('.quick-access-modal').removeClass('active');
}
function closeMobileMenu(selector){
	$('body').removeClass('activeMobileMenu');
	$(selector).closest('.mobile_hidden_links').removeClass('active');

}